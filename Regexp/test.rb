pattern = /(\w+)@([a-z0-9.]+)?/

pattern =~ "goodtime683@gmail.com"

p($1)

p($2)

#str = "正規表現は難しい！ なんて難しいんだ！"

#p str.gsub(/難しい/,"簡単だ").gsub(/難しいんだ/,"簡単なんだ")

#3
def word_capitalize(str)
  result = str.gsub(/\-[a-z]/) do |matched|
    matched.upcase
  end
  return result
end

def word_capitalize2(str)
  str.split(/\-/).collect{|w| w.capitalize}.join("-")
end

p word_capitalize("fa-fa")