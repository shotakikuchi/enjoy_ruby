pattern1 = /ABC/
pattern2 = /^ABC$/

pattern3 = /[ABC]/
pattern4 = /a[ABC]e/

# \sは空白 タブ 改行文字にマッチ
pattern5 = /ABC\sDEF/

#\dは0 - 9までの数字にマッチ
pattern6 = /\d/

#\wは英数字にマッチ
pattern7 = /\w/
pattern8 = /\w\w/

# \Aは文字列の先頭にマッチ
pattern9 = /\A/
pattern9_sub = /\AAAA/

# \zは文字列の末尾にマッチ
pattern10 = /\z/
pattern10_sub = /AAA\z/

string1 = "ABC"
string2 = "ABCEFG"
string3 = "A"




if pattern2 =~ string1
  p("pattern2です")

else pattern1 =~ string1
  p("pattern1です")
end

if pattern3 =~ string3

  print(patern3です)
end



