#a = Array.new
a = (1..100).to_a
# 1.upto(100) do |i|
#   a.push(i)
# end


a = (1..100).to_a

a2 = a.collect{|i| i * 100}
p a2

a.collect!{|i| i * 100}



a = (1..100).to_a

a3 = a.reject{|i| i != 3}
a4 = a.select{|i| i == 3}

a5 = a.reject{|i| i % 3 == 0}


a = (1..100).to_a
a2 = a.reverse

a2 = a.sort{|n1,n2| n2 <=> n1}
a2 = a.sort_by{|i| -i}

p a2


ret = 0
a = (1..100).to_a
a.each do |i|
  ret += i
end

p ret


ary = (1..100).to_a

result = Array.new
10.times do |i|
  result << ary[i*10,10]
end

p result

def sum_array(ary1,ary2)
  result = 0
  i = 0
  ary1.each do |elem1|
    result = elem1 + ary2[i]
    i += 1
  end
end

def sum_array_zip(ary1,ary2)
  result = Array.new
  ary1.zip(ary2){|a,b| result << a + b}
  return result
end

p sum_array_zip([1,2,3],[4,5,6])

