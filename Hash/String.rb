str = "Ruby is an object oriented programming language"

str_ary = str.split

p str_ary.sort

str_ary.sort_by!{|s| s.downcase}

count = Hash.new

str.each_char do |c|
  count[c] = 0 unless count[c]
  count[c] += 1
end

count.keys.sort.each do |c|
  printf("'%s':%s\n",c,"*"*count[c])
end

def kan2num(string)
  digit4 = digit3 = digit2 = digit1 = 0
  nbstring = string.dup

  nbstring.gsub!(/一/,"1")
  nbstring.gsub!(/二/,"2")
  nbstring.gsub!(/三/,"3")
  nbstring.gsub!(/四/,"4")
  nbstring.gsub!(/五/,"5")
  nbstring.gsub!(/六/,"6")
  nbstring.gsub!(/七/,"7")
  nbstring.gsub!(/八/,"8")
  nbstring.gsub!(/九/,"9")

  if nbstring =~ /((\d)?千)?((\d)?百)?((\d)?十)?(\d)?$/

    if $1
      digit4 = $2 || 1
    end
    if $3
      digit3 = $4 || 1
    end

    if $5
      digit2 = $6 || 1
    end

    digit1 = $7 || 0
  end

  return (digit4+digit3+digit2+digit1).to_i

end

p kan2num("一千二百五十三")
p kan2num("五千二百三十七")
