class Exp
  def copy(from, copy)
    src = File.open(from)
    begin
      dst = File.open(to, "w")
      data = src.read
      dst.write(data)
      dst.close
    ensure
      src.close
    end
  end

  def try
    file = ARGV[0]
    begin
      io = File.open(file)
    rescue
      sleep 10
      retry
    end

    data = io.read
    io.close
  end


end