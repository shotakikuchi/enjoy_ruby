class Point
  attr_accessor :x, :y

  def initialize(x, y)
    @x, @y = x, y
  end

  def inspect
    "(#{x},#{y})"
  end

  def +(other)
    self.class.new(x + other.x, y + other.y)
  end

  def -(other)
    self.class.new(x - other.x, y - other.y)
  end

  def +@
    dup
  end

  def -@
    self.class.new(-x, -y)
  end

  def ~@
    self.class.new(-y, x)
  end

  def [](index)
    case index
      when 0
        x
      when 1
        y
      else
        return false
    end
  end

  def []=(index,value)
    case index
      when 0
        self.x = value
      when 1
        self.y = value
      else
        return false
    end
  end
end


point0 = Point.new(3, 6)

point1 = Point.new(2, 8)

p point0 + point1
p point0 - point1

p +point0
p -point0
p ~point0

p point0[0]

p point0[1]

p point0[1] = 4
