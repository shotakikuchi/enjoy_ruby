wday = {sunday:"日曜日", monday:"月曜日", saturday:"土曜日"}

p wday[:sunday]

print(wday.size)


wday.each do |key,value|
  p("#{key}は#{value}のことです")
end

test_strings = "blue 青 white 白\n red 赤"

def str2hash(input_array)
  result = Hash.new(0)
  input_array = input_array.split(/\s+/)
  while key = input_array.shift
    result[key] = input_array.shift
  end
  return result
end

result = str2hash(test_strings)

p(result)

#keyにblackはないのでデフォルト値の0が返る
p(result["black"])