class HelloCount

  @@count = 0

  def self.count  #staticメソッドのようなもの
    @@count
  end

  def initialize(myname = "Ruby")
    @name = myname
  end

  def hello
    @@count += 1
    puts "Hello, world I am #{@name}. \n"
  end

end

bob = HelloCount.new("bob")
alice = HelloCount.new("alice")
ruby = HelloCount.new

p HelloCount.count # => 0

bob.hello
alice.hello
ruby.hello
p HelloCount.count


