module HelloModule  #module文
  Version = "1.0"   #定数の定義

  def foo
    p self
  end

  def hello(name)   #メソッドの定義
    puts "Hello #{name}."
  end
  module_function :hello, :foo    #helloをmodule関数として公開する
end

p HelloModule::Version # => "1.0"

HelloModule.hello("Alice") #=> Hello, Alice

include HelloModule
p Version
hello("Alice")

foo