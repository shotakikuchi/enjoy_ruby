puts "case:break"

langs = ["perl","ruby","php","java","javascript"]
i = 0

langs.each do |lang|
  i += 1
  if i== 3
    break
  end
  p lang,i
end

puts "case:next"
i = 0
langs.each do |lang|
  i += 1
  if i== 3
    next
  end
  p lang,i
end