module MyMobule
  # 共通して提供したいメソッドなど
end

class MyClass1
  include MyMobule
  # MyClass1に固有のメソッドなど
end

class MyClass2
  include MyMobule
  # MyClass2に固有のメソッドなど
end

include Math

p FileTest.exist?("/usr/bin/ruby")

p FileTest.size("/usr/bin/ruby")

p PI

p sqrt(2)