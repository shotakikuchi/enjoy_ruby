class C1
  def hello
    return "Hello"
  end
end

class C2 < C1
  alias old_hello hello

  def hello   #helloを再定義
    "#{old_hello}, again"
  end

  undef hello #helloメソッドをなかったことにする

end

obj = C2.new

p obj.old_hello
p obj.hello