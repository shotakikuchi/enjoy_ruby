 def hello(name = "newBie")
   puts "hello #{name}"
 end

 hello();
 hello("shota")

 def volume(x,y,z)
   return x * y * z
 end

 p volume(2,3,4)

 def myloop
   while true
     yield
   end
 end

 num = 1
 myloop do

   print "num is #{num}"
   break if num > 10
   num *= 2
 end