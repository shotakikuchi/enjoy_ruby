class HelloWorld

  def initialize(name = "Ruby")
    @myname = name
  end

  Version = "2.1.4"
  class << self
    def hello(name)
      p "#{name} , Hello "
    end
  end

  def self.hello(name)
    puts "hello #{name}"
  end

  attr_accessor :myname

  def hello
    puts "Hello World I'm #{self.myname}"
  end

  def change(name)
    self.myname = name
  end


  bob = HelloWorld.new("bob")
  alice = HelloWorld.new("alice")

  bob.hello

  p bob.myname

  p bob.myname = "bobobo"

  bob.change("change")

  p bob.myname


end

p HelloWorld::Version


