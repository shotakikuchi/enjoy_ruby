module M
  def meth
    "meth"
  end
end

class C
  include M #module M をインクルードする
end

C.include?(M)

c = C.new

p c.meth

p C.ancestors

p C.superclass