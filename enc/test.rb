# encoding: utf-8

def to_utf8(str_euc, str_sjis)
  _str_euc = str_euc.encode("utf-8")
  _str_sjis = str_sjis.encode("utf-8")
  result = _str_sjis + _str_euc
  return result
end

## 以下のように実行します。
str_euc  = "こんにちは".encode("EUC-JP")
str_sjis = "さようなら".encode("Shift_JIS")

puts to_utf8(str_euc, str_sjis)