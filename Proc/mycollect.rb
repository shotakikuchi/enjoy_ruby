def mycollect(obj, &block)
  result = []
    obj.each do |i|
      result << block.call(i)
    end
  result
end

ary = mycollect([2, 3, 4, 5, 6]) do |i|
  i * 2
end

p ary