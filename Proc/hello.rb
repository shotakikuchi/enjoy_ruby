hello1 = Proc.new do |name|
  puts "Hello #{name}."
end

hello2 = proc do |name|
  puts "hello #{name}"
end

hello1.call("Hello")

hello2.call("Hello")