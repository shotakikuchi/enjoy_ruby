def aaa(from, to)
  fizz = proc { |n| n % 3 == 0 }
  buzz = proc { |n| n % 5 == 0 }
  fizz_buzz = proc { |n| n % 15 == 0 }
  from.upto(to) do |n|
    case n
      when fizz_buzz
        puts "#{n}:Fizz_Buzz"
      when fizz
        puts "#{n}:Fizz"
      when buzz
        puts "#{n}:Buzz"
    end
  end
end

aaa(1, 20)